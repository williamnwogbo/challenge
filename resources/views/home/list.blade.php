@extends('mainLayout')

@section('content')

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 1?>
                @if($members->count() > 0)
                    @foreach($members as $member)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $member->name }}</td>
                            <td>{{ $member->email }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop