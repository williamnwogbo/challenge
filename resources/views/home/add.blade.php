@extends('mainLayout')

@section('content')

<div class="container">
    <div class="row">
        @include('errors.showerrors')
        <form action="{{ url('/add') }}" method="post">
            {{ csrf_field() }}
            <label>Name</label>
            <input type="text" value="{{ old('name') }}" class="form-control" name="name">
            <label>Email</label>
            <input type="text"  value="{{ old('email') }}" class="form-control" name="email"><br/>
            <input type="submit" class="btn btn-info"/>
        </form>
    </div>
</div>
    @stop