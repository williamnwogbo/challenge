<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    /**
     * Generated
     */

    protected $table = 'members';
    protected $fillable = ['id', 'name', 'email'];



}
