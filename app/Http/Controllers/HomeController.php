<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddMember;
use App\Member;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function add(){
        return view('home.add');
    }

    public function p_add(AddMember $request){
        //convert to an array
        $request_data = $request->all();
        //save the data in the data base
        Member::create($request_data);
        session()->flash('alert-success','Member was saved successfully');
        return back();
    }

    public function lists(){
        $members = Member::all();
        return view('home.list')->with(compact('members'));
    }
}
